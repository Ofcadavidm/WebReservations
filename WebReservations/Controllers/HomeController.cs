﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;

namespace WebReservations.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getReservationList(string sortColumnName = "FlightNo", string sortOrder = "asc", int pageSize = 3, int currentPage = 1 , string searchText = "")
        {
            List<Reservation> List = new List<Reservation>();
            int totalPage = 0;
            int totalRecord = 0;

            using (MyDatabaseEntities dc = new MyDatabaseEntities())
            {
                
                var res = dc.Reservations.Select(a => a);
                //Search
                if (!string.IsNullOrEmpty(searchText))
                {
                    res = res.Where(a => a.Departure.Contains(searchText) || a.Arrival.Contains(searchText) || a.DepartureTime.Contains(searchText) || a.FlightNo.Contains(searchText) || a.FlightDate.Contains(searchText) || a.Dni.Contains(searchText) || a.FirstName.Contains(searchText) || a.LastName.Contains(searchText));
                }
                totalRecord = res.Count();
                if (pageSize > 0)
                {
                    totalPage = totalRecord / pageSize + ((totalRecord % pageSize) > 0 ? 1 : 0);
                    List = res.OrderBy(sortColumnName + " " + sortOrder).Skip(pageSize * (currentPage - 1)).Take(pageSize).ToList();
                }
                else
                {
                    List = res.ToList();
                }
            }

            return new JsonResult
            {             
                Data = new { List = List, totalPage = totalPage, sortColumnName = sortColumnName, sortOrder = sortOrder, currentPage = currentPage, pageSize = pageSize, searchText = searchText },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}